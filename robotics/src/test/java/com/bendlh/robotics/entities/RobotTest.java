package com.bendlh.robotics.entities;

import com.bendlh.robotics.entities.robot.Robot;
import com.bendlh.robotics.utils.TestLogger;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * RobotApp
 * RobotTest
 *
 * Created on 22/07/2017
 * Copyright (c) 2017 SHAPE A/S. All rights reserved.
 */
public class RobotTest {

    @Test
    public void Robot_turnLeft() throws Exception {

        Robot robot = new Robot(0, 0, Robot.DIRECTION_SOUTH);

        robot.turnLeft();
        TestLogger.info(this, robot.getDirectionAsString());
        assertTrue(robot.getDirection() == Robot.DIRECTION_EAST);

        robot.turnLeft();
        TestLogger.info(this, robot.getDirectionAsString());
        assertTrue(robot.getDirection() == Robot.DIRECTION_NORTH);

        robot.turnLeft();
        TestLogger.info(this, robot.getDirectionAsString());
        assertTrue(robot.getDirection() == Robot.DIRECTION_WEST);

        robot.turnLeft();
        TestLogger.info(this, robot.getDirectionAsString());
        assertTrue(robot.getDirection() == Robot.DIRECTION_SOUTH);
    }

    @Test
    public void Robot_turnRight() throws Exception {

        Robot robot = new Robot(0, 0, Robot.DIRECTION_SOUTH);

        robot.turnRight();
        TestLogger.info(this, robot.getDirectionAsString());
        assertTrue(robot.getDirection() == Robot.DIRECTION_WEST);

        robot.turnRight();
        TestLogger.info(this, robot.getDirectionAsString());
        assertTrue(robot.getDirection() == Robot.DIRECTION_NORTH);

        robot.turnRight();
        TestLogger.info(this, robot.getDirectionAsString());
        assertTrue(robot.getDirection() == Robot.DIRECTION_EAST);

        robot.turnRight();
        TestLogger.info(this, robot.getDirectionAsString());
        assertTrue(robot.getDirection() == Robot.DIRECTION_SOUTH);
    }

    @Test
    public void Robot_goForward() throws Exception {

        Robot robot = new Robot(0, 0, Robot.DIRECTION_SOUTH);

        robot.goForward();
        TestLogger.info(this, "y = " + robot.getY());
        assertTrue(robot.getY() == 1);

        robot.turnLeft();
        robot.goForward();
        TestLogger.info(this, "x = " + robot.getX());
        assertTrue(robot.getX() == 1);

        robot.turnLeft();
        robot.goForward();
        TestLogger.info(this, "y = " + robot.getY());
        assertTrue(robot.getY() == 0);

        robot.turnLeft();
        robot.goForward();
        TestLogger.info(this, "x = " + robot.getX());
        assertTrue(robot.getX() == 0);
    }
}