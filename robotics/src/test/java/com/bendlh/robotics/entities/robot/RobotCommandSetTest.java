package com.bendlh.robotics.entities.robot;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * RobotApp
 * RobotCommandSetTest
 *
 * Created on 22/07/2017
 * Copyright (c) 2017 SHAPE A/S. All rights reserved.
 */
public class RobotCommandSetTest {

    @Test
    public void RobotCommandSet_test() throws Exception {

        Robot robot = new Robot(0, 0, Robot.DIRECTION_SOUTH);
        RobotCommandSet commandSet = new RobotCommandSet(RobotCommandParser.parseCommands("LLRRFF"));

        // First command - L
        assertTrue(commandSet.hasCommands());
        commandSet.executeNextCommand(robot);
        assertTrue(robot.getDirection() == Robot.DIRECTION_EAST);

        // Second command - L
        assertTrue(commandSet.hasCommands());
        commandSet.executeNextCommand(robot);
        assertTrue(robot.getDirection() == Robot.DIRECTION_NORTH);

        // Third command - R
        assertTrue(commandSet.hasCommands());
        commandSet.executeNextCommand(robot);
        assertTrue(robot.getDirection() == Robot.DIRECTION_EAST);

        // Forth command - R
        assertTrue(commandSet.hasCommands());
        commandSet.executeNextCommand(robot);
        assertTrue(robot.getDirection() == Robot.DIRECTION_SOUTH);

        // Fifth command - F
        assertTrue(commandSet.hasCommands());
        commandSet.executeNextCommand(robot);
        assertTrue(robot.getY() == 1);

        // Sixth command - F
        assertTrue(commandSet.hasCommands());
        commandSet.executeNextCommand(robot);
        assertTrue(robot.getY() == 2);

        // Make sure we have no commands left
        assertTrue(!commandSet.hasCommands());
    }
}