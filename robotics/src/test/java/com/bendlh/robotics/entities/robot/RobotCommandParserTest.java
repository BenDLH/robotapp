package com.bendlh.robotics.entities.robot;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * RobotApp
 * RobotCommandParserTest
 *
 * Created on 22/07/2017
 * Copyright (c) 2017 SHAPE A/S. All rights reserved.
 */
public class RobotCommandParserTest {

    @Test
    public void RobotCommandParser_parseCommands() throws Exception {

        List<Integer> commands = RobotCommandParser.parseCommands("LLRRFF");

        assertTrue(commands.get(0) == Robot.COMMAND_TURN_LEFT);
        assertTrue(commands.get(1) == Robot.COMMAND_TURN_LEFT);
        assertTrue(commands.get(2) == Robot.COMMAND_TURN_RIGHT);
        assertTrue(commands.get(3) == Robot.COMMAND_TURN_RIGHT);
        assertTrue(commands.get(4) == Robot.COMMAND_GO_FORWARD);
        assertTrue(commands.get(5) == Robot.COMMAND_GO_FORWARD);
    }

    @Test
    public void RobotCommandParser_isValidCommandString() throws Exception {

        boolean isValid = RobotCommandParser.isValidCommandString("LLRRFF");
        assertTrue(isValid);

        isValid = RobotCommandParser.isValidCommandString("RFLFLRLFL");
        assertTrue(isValid);

        isValid = RobotCommandParser.isValidCommandString("LLRRFFa");
        assertTrue(!isValid);
    }
}