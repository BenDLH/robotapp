package com.bendlh.robotics.entities.robot;

import android.support.annotation.NonNull;

import java.util.List;

/**
 * RobotApp
 * RobotCommandSet
 *
 * Created on 22/07/2017
 * Copyright (c) 2017 SHAPE A/S. All rights reserved.
 */
public class RobotCommandSet {

    @NonNull
    private final List<Integer> _commands;

    private int _currentCommand = 0;

    public RobotCommandSet(@NonNull List<Integer> commands) {
        _commands = commands;
    }

    public void executeNextCommand(Robot robot) {

        switch (_commands.get(_currentCommand)) {

            case Robot.COMMAND_TURN_LEFT:
                robot.turnLeft();
                break;

            case Robot.COMMAND_TURN_RIGHT:
                robot.turnRight();
                break;

            case Robot.COMMAND_GO_FORWARD:
                robot.goForward();
                break;
        }

        _currentCommand++;
    }

    public boolean hasCommands() {
        return _currentCommand < _commands.size();
    }
}
