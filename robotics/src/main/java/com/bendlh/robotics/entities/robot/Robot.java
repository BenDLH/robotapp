package com.bendlh.robotics.entities.robot;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * RobotApp
 * Robot
 *
 * Created on 22/07/2017
 * Copyright (c) 2017 SHAPE A/S. All rights reserved.
 */
public class Robot {

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({DIRECTION_NORTH, DIRECTION_EAST, DIRECTION_SOUTH, DIRECTION_WEST})
    public @interface Direction {}
    public static final int DIRECTION_NORTH = 0;
    public static final int DIRECTION_EAST = 1;
    public static final int DIRECTION_SOUTH = 2;
    public static final int DIRECTION_WEST = 3;

    public static final char DIRECTION_NORTH_CHAR = 'n';
    public static final char DIRECTION_EAST_CHAR = 'e';
    public static final char DIRECTION_SOUTH_CHAR = 's';
    public static final char DIRECTION_WEST_CHAR = 'w';

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({COMMAND_TURN_LEFT, COMMAND_TURN_RIGHT, COMMAND_GO_FORWARD})
    public @interface Command {}
    public static final int COMMAND_TURN_LEFT = 0;
    public static final int COMMAND_TURN_RIGHT = 1;
    public static final int COMMAND_GO_FORWARD = 2;

    private int _startX;
    private int _startY;
    private @Direction int _startDirection;

    private int _x;
    private int _y;
    private @Direction int _direction;

    public Robot(int startX, int startY, @Direction int direction) {
        _startX = _x = startX;
        _startY = _y = startY;
        _startDirection = _direction = direction;
    }

    // region Getters

    public int getX() {
        return _x;
    }

    public int getY() {
        return _y;
    }

    public int getDirection() {
        return _direction;
    }

    public String getDirectionAsString() {
        return getDirectionAsString(_direction);
    }

    // endregion

    // region Setters

    public void setStartPosition(int x, int y, @Direction int direction) {
        _startX = _x = x;
        _startY = _y = y;
        _startDirection = _direction = direction;
    }

    // endregion

    // region Commands

    public void resetToStart() {
        _x = _startX;
        _y = _startY;
        _direction = _startDirection;
    }

    public void turnLeft() {
        _direction = getAsDirection(floorModulus(_direction - 1, 4));
    }

    public void turnRight() {
        _direction = getAsDirection(floorModulus(_direction + 1, 4));
    }

    public void goForward() {
        switch (_direction) {
            case DIRECTION_NORTH: _y--; break;
            case DIRECTION_EAST: _x++; break;
            case DIRECTION_SOUTH: _y++; break;
            case DIRECTION_WEST: _x--; break;
        }
    }

    // endregion

    // TODO Better way to solve this
    public static boolean isValidDirection(char direction) {

        // TODO Could be solved by ASCII mapping instead for better efficiency
        direction = ("" + direction).toLowerCase().charAt(0);

        switch (direction) {
            case DIRECTION_NORTH_CHAR: return true;
            case DIRECTION_EAST_CHAR: return true;
            case DIRECTION_SOUTH_CHAR: return true;
            case DIRECTION_WEST_CHAR: return true;
            default: return false;
        }
    }

    // TODO Better way to solve this
    public static int parseDirection(char direction) {
        switch (direction) {
            case 'n': return DIRECTION_NORTH;
            case 'e': return DIRECTION_EAST;
            case 's': return DIRECTION_SOUTH;
            case 'w': return DIRECTION_WEST;
            default: return DIRECTION_NORTH;
        }
    }

    @Direction
    private int getAsDirection(int direction) {
        switch (direction) {
            case DIRECTION_NORTH: return DIRECTION_NORTH;
            case DIRECTION_EAST: return DIRECTION_EAST;
            case DIRECTION_SOUTH: return DIRECTION_SOUTH;
            case DIRECTION_WEST: return DIRECTION_WEST;
            default: return DIRECTION_NORTH;
        }
    }

    private String getDirectionAsString(int direction) {
        switch (direction) {
            case DIRECTION_NORTH: return "NORTH";
            case DIRECTION_EAST: return "EAST";
            case DIRECTION_SOUTH: return "SOUTH";
            case DIRECTION_WEST: return "WEST";
            default: return "UNKNOWN";
        }
    }

    /**
     * We need a modulus that wraps to positive when dealing with negative numbers
     */
    private int floorModulus(int a, int b) {
        return (a % b + b) % b;
    }
}
