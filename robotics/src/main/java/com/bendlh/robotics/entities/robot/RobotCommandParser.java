package com.bendlh.robotics.entities.robot;

import java.util.ArrayList;
import java.util.List;

/**
 * RobotApp
 * RobotCommandParser
 *
 * Created on 22/07/2017
 * Copyright (c) 2017 SHAPE A/S. All rights reserved.
 */
public class RobotCommandParser {

    public static List<Integer> parseCommands(String commandString) {

        // Ensure lower case
        commandString = commandString.toLowerCase();

        List<Integer> commands = new ArrayList<>();

        for (int index = 0; index < commandString.length(); index++) {

            char character = commandString.charAt(index);

            switch (character) {
                case 'l': commands.add(Robot.COMMAND_TURN_LEFT); break;
                case 'r': commands.add(Robot.COMMAND_TURN_RIGHT); break;
                case 'f': commands.add(Robot.COMMAND_GO_FORWARD); break;
            }
        }

        return commands;
    }

    public static boolean isValidCommandString(String commandString) {

        // Drop to lower
        commandString = commandString.toLowerCase();

        // TODO Regex
        boolean isInvalidCommand;
        for (int index = 0; index < commandString.length(); index++) {
            isInvalidCommand = !(commandString.charAt(index) == 'l' || commandString.charAt(index) == 'r' || commandString.charAt(index) == 'f');
            if (isInvalidCommand) return false;
        }

        return true;
    }
}
