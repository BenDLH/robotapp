package com.bendlh.robotics.utils;

/**
 * TestLogger
 * Logger
 *
 * Created on 22/07/2017
 * Copyright (c) 2017 SHAPE A/S. All rights reserved.
 */
public class TestLogger {

    public static void debug(Object object, String message) {
        System.out.println("debug: " + object.getClass().getSimpleName() + " ===> " + message);
    }

    public static void error(Object object, String message) {
        System.out.println("error: " + object.getClass().getSimpleName() + " ===> " + message);
    }

    public static void error(Object object, Throwable error) {
        System.out.println("error: " + object.getClass().getSimpleName() + " ===> " + error.toString());
    }

    public static void info(Object object, String message) {
        System.out.println("info: " + object.getClass().getSimpleName() + " ===> " + message);
    }
}