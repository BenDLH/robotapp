package com.bendlh.robotics.utils;

import android.util.Log;

import com.bendlh.robotics.BuildConfig;

/**
 * RobotApp
 * Logger
 *
 * Created on 22/07/2017
 * Copyright (c) 2017 SHAPE A/S. All rights reserved.
 */
public class Logger {

    public static void debug(Object object, String message) {
        if(BuildConfig.DEBUG) {
            Log.d(object.getClass().getSimpleName(), message);
        }
    }

    public static void error(Object object, String message) {
        Log.e(object.getClass().getSimpleName(), message);
    }

    public static void error(Object object, Throwable error) {
        Log.e(object.getClass().getSimpleName(), error.toString());
        Log.e(object.getClass().getSimpleName(), "Printing stack trace:");
        error.printStackTrace();
    }

    public static void info(Object object, String message) {
        if (BuildConfig.DEBUG) {
            Log.i(object.getClass().getSimpleName(), message);
        }
    }
}