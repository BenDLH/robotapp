package com.bendlh.robotapp.viewmodels;

import android.view.View;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * RobotApp
 * SetupDialogViewModelTest
 *
 * Created on 23/07/2017
 * Copyright (c) 2017 SHAPE A/S. All rights reserved.
 */
public class SetupDialogViewModelTest implements SetupDialogViewModel.Listener {

    @Test
    public void SetupDialogViewModel_isValidRoomWidth() throws Exception {

        SetupDialogViewModel viewModel = new SetupDialogViewModel(this);

        assertFalse(viewModel.isValidRoomWidth(0));

        assertTrue(viewModel.isValidRoomWidth(1));
        assertTrue(viewModel.isValidRoomWidth(4));
        assertTrue(viewModel.isValidRoomWidth(100));
    }

    @Test
    public void SetupDialogViewModel_isValidRoomHeight() throws Exception {

        SetupDialogViewModel viewModel = new SetupDialogViewModel(this);

        assertFalse(viewModel.isValidRoomHeight(0));

        assertTrue(viewModel.isValidRoomHeight(1));
        assertTrue(viewModel.isValidRoomHeight(4));
        assertTrue(viewModel.isValidRoomHeight(100));
    }

    @Test
    public void SetupDialogViewModel_isValidStartX() throws Exception {

        SetupDialogViewModel viewModel = new SetupDialogViewModel(this);

        assertFalse(viewModel.isValidStartX(10, 9));
        assertFalse(viewModel.isValidStartX(0, 5));

        assertTrue(viewModel.isValidStartX(4, 5));
        assertTrue(viewModel.isValidStartX(1, 100));
    }

    @Test
    public void SetupDialogViewModel_isValidStartY() throws Exception {

        SetupDialogViewModel viewModel = new SetupDialogViewModel(this);

        assertFalse(viewModel.isValidStartY(10, 9));
        assertFalse(viewModel.isValidStartY(0, 5));

        assertTrue(viewModel.isValidStartY(4, 5));
        assertTrue(viewModel.isValidStartY(1, 100));
    }

    @Test
    public void SetupDialogViewModel_isValidStartDirection() throws Exception {

        SetupDialogViewModel viewModel = new SetupDialogViewModel(this);

        assertFalse(viewModel.isValidStartDirection('x'));

        assertTrue(viewModel.isValidStartDirection('N'));

        assertTrue(viewModel.isValidStartDirection('n'));
        assertTrue(viewModel.isValidStartDirection('e'));
        assertTrue(viewModel.isValidStartDirection('s'));
        assertTrue(viewModel.isValidStartDirection('w'));
    }

    @Override
    public void updateSetup(int roomWidth, int roomHeight, int startX, int startY, char startDirection) {}

    @Override
    public void showKeyboard(View view) {}

    @Override
    public void dismiss() {}
}