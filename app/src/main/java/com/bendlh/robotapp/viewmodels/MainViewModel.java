package com.bendlh.robotapp.viewmodels;

import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;

import com.bendlh.robotapp.CustomApplication;
import com.bendlh.robotapp.R;
import com.bendlh.robotapp.databinding.ViewMainBinding;
import com.bendlh.robotapp.utils.Toaster;
import com.bendlh.robotics.entities.Room;
import com.bendlh.robotics.entities.robot.Robot;
import com.bendlh.robotics.entities.robot.RobotCommandParser;
import com.bendlh.robotics.entities.robot.RobotCommandSet;

/**
 * RobotApp
 * MainViewModel
 *
 * Created on 22/07/2017
 * Copyright (c) 2017 SHAPE A/S. All rights reserved.
 */
public class MainViewModel {

    public interface Listener {
        void showSetupRoomDialog();
        void hideKeyboard();
    }

    public final ObservableField<Room> room = new ObservableField<>();
    public final ObservableField<Robot> robot = new ObservableField<>();

    public final ObservableField<String> commandText = new ObservableField<>();
    public final ObservableField<Integer> commandTextColor = new ObservableField<>(getColor(R.color.text));
    public final ObservableBoolean commandInputEnabled = new ObservableBoolean(true);

    private Listener _listener;

    private ViewMainBinding _binding;

    public MainViewModel(Listener listener) {
        _listener = listener;

        room.set(new Room(5, 5));
        robot.set(new Robot(0, 0, Robot.DIRECTION_SOUTH));
    }

    public View inflateView(LayoutInflater inflater) {
        _binding = ViewMainBinding.inflate(inflater);
        _binding.setViewModel(this);
        return _binding.getRoot();
    }

    public void setRoom(@NonNull Room room) {
        this.room.set(room);
    }

    public void setRobot(Robot robot) {
        this.robot.set(robot);
    }

    public void updateSetup(int roomWidth, int roomHeight, int startX, int startY, char startDirection) {

        Room room = this.room.get();
        room.setWidth(roomWidth);
        room.setHeight(roomHeight);

        Robot robot = this.robot.get();
        robot.setStartPosition(startX, startY, Robot.parseDirection(startDirection));

        _binding.roomView.invalidate();
    }

    public void onRoomClicked(View view) {
        this.robot.get().resetToStart();
        _binding.roomView.invalidate();
    }

    public void onSetupClicked(View view) {
        _listener.showSetupRoomDialog();
    }

    public void onCommandTextChanged(CharSequence text, int start, int before, int count) {
        commandTextColor.set(getColor(RobotCommandParser.isValidCommandString(text.toString()) ? R.color.text : R.color.red));
    }

    public void onSendCommandsClicked(View view) {
        if (!commandInputEnabled.get()) return;

        if (!RobotCommandParser.isValidCommandString(commandText.get())) {
            Toaster.makeShort("Please supply valid commands (L, R, or F)");
            return;
        }

        _listener.hideKeyboard();
        commandInputEnabled.set(false);
        RobotCommandSet commandSet = new RobotCommandSet(RobotCommandParser.parseCommands(commandText.get()));
        runNextCommand(commandSet, robot.get());
    }

    private void runNextCommand(RobotCommandSet commandSet, Robot robot) {
        if (!commandSet.hasCommands()) {
            commandInputEnabled.set(true);
            commandText.set("");
            Toaster.makeShort("Robot position: X:" + (robot.getX() + 1) + ", Y:" + (robot.getY() + 1) + ", Direction:" + robot.getDirectionAsString());
            return;
        }

        commandSet.executeNextCommand(robot);
        _binding.roomView.invalidate();

        _binding.getRoot().postDelayed(() -> runNextCommand(commandSet, robot), 500);
    }

    private int getColor(@ColorRes int colorResource) {
        return ContextCompat.getColor(CustomApplication.get(), colorResource);
    }
}
