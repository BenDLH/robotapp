package com.bendlh.robotapp.viewmodels;

import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.support.annotation.ColorRes;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;

import com.bendlh.robotapp.CustomApplication;
import com.bendlh.robotapp.R;
import com.bendlh.robotapp.databinding.ViewSetupDialogBinding;
import com.bendlh.robotics.entities.robot.Robot;

/**
 * RobotApp
 * SetupDialogViewModel
 *
 * Created on 23/07/2017
 * Copyright (c) 2017 SHAPE A/S. All rights reserved.
 */
public class SetupDialogViewModel {

    public interface Listener {
        void updateSetup(int roomWidth, int roomHeight, int startX, int startY, char startDirection);
        void showKeyboard(View view);
        void dismiss();
    }

    public ObservableField<String> roomWidthText = new ObservableField<>();
    public ObservableInt roomWidthTextColor = new ObservableInt();
    private int roomWidth;

    public ObservableField<String> roomHeightText = new ObservableField<>();
    public ObservableInt roomHeightTextColor = new ObservableInt();
    private int roomHeight;

    public ObservableField<String> robotStartXText = new ObservableField<>();
    public ObservableInt robotStartXTextColor = new ObservableInt();
    private int robotStartX;

    public ObservableField<String> robotStartYText = new ObservableField<>();
    public ObservableInt robotStartYTextColor = new ObservableInt();
    private int robotStartY;

    public ObservableField<String> robotStartDirectionText = new ObservableField<>();
    public ObservableInt robotStartDirectionTextColor = new ObservableInt();
    private char robotStartDirection;

    public ObservableBoolean updateButtonEnabled = new ObservableBoolean();

    private final Listener _listener;

    private ViewSetupDialogBinding _binding;

    public SetupDialogViewModel(Listener listener) {
        _listener = listener;
    }

    public View inflateView(LayoutInflater inflater) {
        _binding = ViewSetupDialogBinding.inflate(inflater);
        _binding.setViewModel(this);

        _listener.showKeyboard(_binding.roomWidthText);

        return _binding.getRoot();
    }

    public void onRoomWidthTextChanged(CharSequence text, int start, int before, int count) {
        roomWidth = parseInt(text.toString());
        onFieldChanged();
    }

    public void onRoomHeightTextChanged(CharSequence text, int start, int before, int count) {
        roomHeight = parseInt(text.toString());
        onFieldChanged();
    }

    public void onRobotStartXTextChanged(CharSequence text, int start, int before, int count) {
        robotStartX = parseInt(text.toString()) - 1; // UI is indexed from 1
        onFieldChanged();
    }

    public void onRobotStartYTextChanged(CharSequence text, int start, int before, int count) {
        robotStartY = parseInt(text.toString()) - 1; // UI is indexed from 1
        onFieldChanged();
    }

    public void onRobotStartDirectionTextChanged(CharSequence text, int start, int before, int count) {
        robotStartDirection = text.toString().isEmpty() ? 'a' : text.toString().charAt(0);
        onFieldChanged();
    }

    public void onUpdateButtonClicked(View view) {
        _listener.updateSetup(roomWidth, roomHeight, robotStartX, robotStartY, robotStartDirection);
    }

    public void onDismissButtonClicked(View view) {
        _listener.dismiss();
    }

    private void onFieldChanged() {
        updateButtonEnabled.set(
            isValidRoomWidth(roomWidth) && isValidRoomHeight(roomHeight) &&
            isValidStartX(robotStartX, roomWidth) && isValidStartY(robotStartY, roomHeight) &&
            isValidStartDirection(robotStartDirection)
        );

        roomWidthTextColor.set(getColor(isValidRoomWidth(roomWidth) ? R.color.text : R.color.red));
        roomHeightTextColor.set(getColor(isValidRoomHeight(roomHeight) ? R.color.text : R.color.red));

        robotStartXTextColor.set(getColor(isValidStartX(robotStartX, roomWidth) ? R.color.text : R.color.red));
        robotStartYTextColor.set(getColor(isValidStartY(robotStartY, roomHeight) ? R.color.text : R.color.red));
        robotStartDirectionTextColor.set(getColor(isValidStartDirection(robotStartDirection) ? R.color.text : R.color.red));
    }

    public boolean isValidRoomWidth(int roomWidth) {
        return roomWidth > 0;
    }

    public boolean isValidRoomHeight(int roomHeight) {
        return roomHeight > 0;
    }

    public boolean isValidStartX(int startX, int roomWidth) {
        return startX >= 0 && startX < roomWidth;
    }

    public boolean isValidStartY(int startY, int roomHeight) {
        return startY >= 0 && startY < roomHeight;
    }

    public boolean isValidStartDirection(char robotStartDirection) {
        return Robot.isValidDirection(robotStartDirection);
    }

    private int getColor(@ColorRes int colorResource) {
        return ContextCompat.getColor(CustomApplication.get(), colorResource);
    }

    private int parseInt(String text) {
        try {
            return Integer.parseInt(text);
        } catch (NumberFormatException ex) {
            return 0;
        }
    }
}
