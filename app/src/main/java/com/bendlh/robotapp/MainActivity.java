package com.bendlh.robotapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import com.bendlh.robotapp.dialogs.SetupDialog;
import com.bendlh.robotapp.utils.KeyboardUtils;
import com.bendlh.robotapp.viewmodels.MainViewModel;
import com.bendlh.robotapp.viewmodels.SetupDialogViewModel;

/**
 * RobotApp
 * MainActivity
 *
 * Created on 23/07/2017
 * Copyright (c) 2017 SHAPE A/S. All rights reserved.
 */
public class MainActivity extends AppCompatActivity implements MainViewModel.Listener, SetupDialog.Listener {

    private MainViewModel _viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        _viewModel = new MainViewModel(this);
        setContentView(_viewModel.inflateView(LayoutInflater.from(this)));
    }

    @Override
    public void showSetupRoomDialog() {
        new SetupDialog(this, this).show();
    }

    @Override
    public void showKeyboard(View view) {
        KeyboardUtils.showKeyboard(this, view);
    }

    @Override
    public void hideKeyboard() {
        KeyboardUtils.hideKeyboard(this);
    }

    @Override
    public void updateSetup(int roomWidth, int roomHeight, int startX, int startY, char startDirection) {
        _viewModel.updateSetup(roomWidth, roomHeight, startX, startY, startDirection);
    }
}
