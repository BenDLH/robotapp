package com.bendlh.robotapp.utils;

import android.app.Activity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/**
 * RobotApp
 * KeyboardUtils
 *
 * Created on 22/07/2017
 * Copyright (c) 2017 SHAPE A/S. All rights reserved.
 */
public class KeyboardUtils {

    public static void showKeyboard(Activity activity, View focusView) {

        focusView.post(() -> {
            focusView.requestFocus();
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.showSoftInput(focusView, InputMethodManager.SHOW_IMPLICIT);
        });
    }

    public static void hideKeyboard(Activity activity) {

        if (activity.getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
            activity.getCurrentFocus().clearFocus();
        }
    }
}
