package com.bendlh.robotapp.utils;

import android.widget.Toast;

import com.bendlh.robotapp.CustomApplication;

/**
 * RobotApp
 * Toaster
 *
 * Created on 22/07/2017
 * Copyright (c) 2017 SHAPE A/S. All rights reserved.
 */
public class Toaster {

    public static void makeShort(String message) {
        Toast.makeText(CustomApplication.get(), message, Toast.LENGTH_SHORT).show();
    }

    public static void makeLong(String message) {
        Toast.makeText(CustomApplication.get(), message, Toast.LENGTH_LONG).show();
    }
}

