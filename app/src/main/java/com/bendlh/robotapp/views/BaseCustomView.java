package com.bendlh.robotapp.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.os.Build;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.support.annotation.StyleableRes;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;

/**
 * RobotApp
 * BaseCustomView
 *
 * Created on 22/07/2017
 * Copyright (c) 2017 SHAPE A/S. All rights reserved.
 */
public abstract class BaseCustomView extends View {

    private int _generatedWidth;
    private int _generatedHeight;

    protected abstract @StyleableRes
    int[] getAttributesRes();
    protected abstract void init();
    protected abstract void handleAttributes(TypedArray attributes);

    public BaseCustomView(Context context) {
        super(context);
        init();
        initialize(null);
    }

    public BaseCustomView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
        initialize(attrs);
    }

    @CallSuper
    private void initialize(@Nullable AttributeSet attrs) {

        if (attrs != null) {

            TypedArray attributes = getContext().obtainStyledAttributes(attrs, getAttributesRes());
            handleAttributes(attributes);
            attributes.recycle();
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        _generatedWidth = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        _generatedHeight = MeasureSpec.getSize(heightMeasureSpec);

        int finalWidth;
        int finalHeight;

        int desiredWidth = getDesiredWidth();
        int desiredHeight = getDesiredHeight();

        // Measure Width
        switch (widthMode) {

            case MeasureSpec.EXACTLY:
                // Must be this size
                finalWidth = _generatedWidth;
                break;

            case MeasureSpec.AT_MOST:
                // Can't be bigger than...
                finalWidth = Math.min(desiredWidth, _generatedWidth);
                break;

            default:
                // Be whatever you want
                finalWidth = desiredWidth;
        }

        // Measure Height
        switch (heightMode) {

            case MeasureSpec.EXACTLY:
                // Must be this size
                finalHeight = _generatedHeight;
                break;

            case MeasureSpec.AT_MOST:
                // Can't be bigger than...
                finalHeight = Math.min(desiredHeight, _generatedHeight);
                break;

            default:
                // Be whatever you want
                finalHeight = desiredHeight;
        }

        setMeasuredDimension(finalWidth, finalHeight);
    }

    protected void resetPaint(Paint paint) {
        paint.reset();
        paint.setDither(true);
        paint.setAntiAlias(true);
    }

    protected void resetCanvas(Canvas canvas) {
        canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
    }

    protected float dpToPx(int dp) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getResources().getDisplayMetrics());
    }

    protected float pxToDp(int px) {
        DisplayMetrics metrics = getContext().getResources().getDisplayMetrics();
        return px / (metrics.densityDpi / 160f);
    }

    @Override
    public void setElevation(float elevation) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            super.setElevation(elevation);
        } else {
            ViewCompat.setElevation(this, elevation);
        }
    }

    protected int getDesiredWidth() { return _generatedWidth; }

    protected int getDesiredHeight() { return _generatedHeight; }
}