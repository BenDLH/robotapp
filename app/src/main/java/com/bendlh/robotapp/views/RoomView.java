package com.bendlh.robotapp.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;

import com.bendlh.robotapp.R;
import com.bendlh.robotics.entities.Room;
import com.bendlh.robotics.entities.robot.Robot;

/**
 * RobotApp
 * RoomView
 *
 * Created on 22/07/2017
 * Copyright (c) 2017 SHAPE A/S. All rights reserved.
 */
public class RoomView extends BaseCustomView {

    private static final int ROBOT_WIDTH = 80;
    private static final int ROBOT_HEIGHT = 80;

    private int _lineStartX = 0;
    private int _lineStartY = 0;
    private int _lineStopX = 0;
    private int _lineStopY = 0;

    private int _robotFrontXOffset = 0;
    private int _robotFrontYOffset = 0;

    private int _robotColor = ContextCompat.getColor(getContext(), R.color.colorPrimary);
    private int _strokeColor = ContextCompat.getColor(getContext(), R.color.colorAccent);
    private int _backgroundColor = ContextCompat.getColor(getContext(), R.color.colorBackground);

    private int _strokeWidth = 10;

    @NonNull
    private Paint _paint = new Paint(Paint.ANTI_ALIAS_FLAG);

    @NonNull
    private Room _room = new Room(5, 5);

    @Nullable
    private Robot _robot;

    public RoomView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RoomView(Context context) {
        super(context);
    }

    @Override
    protected int[] getAttributesRes() {
        return new int[0];
    }

    @Override
    protected void init() {}

    @Override
    protected void handleAttributes(TypedArray attributes) {}

    // region Drawing

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(getMeasuredWidth(), getMeasuredWidth());
    }

    @Override
    protected void onDraw(@NonNull Canvas canvas) {
        drawGridBackground(canvas);
        drawGrid(canvas);
        if (_robot != null) drawRobot(canvas, _robot);
    }

    private void drawGridBackground(Canvas canvas) {
        _paint.setColor(_backgroundColor);
        canvas.drawRect(0, 0, getWidth(), getHeight(), _paint);
        resetPaint(_paint);
    }

    private void drawGrid(@NonNull Canvas canvas) {

        _paint.setColor(_strokeColor);
        _paint.setStrokeWidth(_strokeWidth);

        // Horizontal lines
        for (int index = 1; index < _room.getWidth(); index++) {

            _lineStartX = getWidth() / _room.getWidth() * index;
            _lineStopX = getWidth() / _room.getWidth() * index;

            canvas.drawLine(_lineStartX, 0, _lineStopX, getWidth(), _paint);
        }

        // Vertical lines
        for (int index = 1; index < _room.getHeight(); index++) {

            _lineStartY = getHeight() / _room.getHeight() * index;
            _lineStopY = getHeight() / _room.getHeight() * index;

            canvas.drawLine(0, _lineStartY, getWidth(), _lineStopY, _paint);
        }

        resetPaint(_paint);
    }

    private void drawRobot(Canvas canvas, @NonNull Robot robot) {

        switch (robot.getDirection()) {

            case Robot.DIRECTION_NORTH:
                _robotFrontXOffset = 0;
                _robotFrontYOffset = -ROBOT_HEIGHT / 4;
                break;

            case Robot.DIRECTION_EAST:
                _robotFrontXOffset = ROBOT_HEIGHT / 4;
                _robotFrontYOffset = 0;
                break;

            case Robot.DIRECTION_SOUTH:
                _robotFrontXOffset = 0;
                _robotFrontYOffset = ROBOT_HEIGHT / 4;
                break;

            case Robot.DIRECTION_WEST:
                _robotFrontXOffset = -ROBOT_HEIGHT / 4;
                _robotFrontYOffset = 0;
                break;
        }

        _paint.setColor(_robotColor);

        canvas.drawRect(
                robot.getX() * getColumnWidth() + (getColumnWidth() / 2) - (ROBOT_WIDTH / 2) - _robotFrontXOffset,
                robot.getY() * getRowHeight() + (getRowHeight() / 2) - (ROBOT_WIDTH / 2) - _robotFrontYOffset,
                robot.getX() * getColumnWidth() + (getColumnWidth() / 2) + (ROBOT_WIDTH / 2) - _robotFrontXOffset,
                robot.getY() * getRowHeight() + (getRowHeight() / 2) + (ROBOT_WIDTH / 2) - _robotFrontYOffset,
                _paint
        );

        canvas.drawCircle(
                robot.getX() * getColumnWidth() + (getColumnWidth() / 2) + _robotFrontXOffset,
                robot.getY() * getRowHeight() + (getRowHeight() / 2) + _robotFrontYOffset,
                (ROBOT_WIDTH / 2),
                _paint
        );

        resetPaint(_paint);
    }

    private int getColumnWidth() {
        return getWidth() / _room.getWidth();
    }

    private int getRowHeight() {
        return getHeight() / _room.getHeight();
    }

    // endregion

    // region Public API

    public void setRoom(@NonNull Room room) {
        _room = room;
        requestLayout();
    }

    public void setRobot(Robot robot) {
        _robot = robot;
        invalidate();
    }

    // endregion
}
