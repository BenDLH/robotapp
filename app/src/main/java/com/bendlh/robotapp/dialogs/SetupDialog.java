package com.bendlh.robotapp.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import com.bendlh.robotapp.viewmodels.SetupDialogViewModel;

/**
 * RobotApp
 * SetupDialog
 *
 * Created on 22/07/2017
 * Copyright (c) 2017 SHAPE A/S. All rights reserved.
 */
public class SetupDialog extends AlertDialog implements SetupDialogViewModel.Listener {

    public interface Listener {
        void updateSetup(int roomWidth, int roomHeight, int startX, int startY, char startDirection);
        void showKeyboard(View view);
    }

    private final SetupDialogViewModel _viewModel;
    private final Listener _listener;

    public SetupDialog(Context context, Listener listener) {
        super(context);

        _listener = listener;
        _viewModel = new SetupDialogViewModel(this);
        setView(_viewModel.inflateView(LayoutInflater.from(context)));
    }

    @Override
    public void updateSetup(int roomWidth, int roomHeight, int startX, int startY, char startDirection) {
        _listener.updateSetup(roomWidth, roomHeight, startX, startY, startDirection);
        dismiss();
    }

    @Override
    public void showKeyboard(View view) {
        _listener.showKeyboard(view);
    }
}
