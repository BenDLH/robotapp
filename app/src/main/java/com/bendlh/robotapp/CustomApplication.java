package com.bendlh.robotapp;

import android.app.Application;
import android.content.Context;

/**
 * RobotApp
 * CustomApplication
 *
 * Created on 22/07/2017
 * Copyright (c) 2017 SHAPE A/S. All rights reserved.
 */
public class CustomApplication extends Application {

    private static CustomApplication _context;

    public static Context get() {
        return _context;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        _context = this;
    }
}
